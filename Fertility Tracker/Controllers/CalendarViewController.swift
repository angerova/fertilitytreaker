

import UIKit
import JTAppleCalendar
import DrawerView

class CalendarViewController: UIViewController, UserUnitsDelegate{

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var drawerContentView: DrawerContentView!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var drawerView: DrawerView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    var selectedDatesFromUD: [Date] = []
    var calendarViewModes: CalendarViewModes = .viewingMode
    var datesInfoDictionary = [String:DateInformation]()
    var dateInformations: [DateInformation] = []
    var selectedDate: Date!
    
    @IBAction func showToday(_ sender: Any) {
        calendarView.scrollToHeaderForDate(Date())
    }
    
    @IBAction func edit(_ sender: Any) {
        if calendarViewModes == .viewingMode{
            calendarView.deselectAllDates(triggerSelectionDelegate: false)
        }else{
            fertilityRanges.selectedPeriodsUD = selectedDatesFromUD
            fertilityRanges.reloadDateInfomations()
        }
        calendarViewModes = (calendarViewModes == .editingMode) ? .viewingMode : .editingMode
        
        calendarViewReloadData(mode: calendarViewModes)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UserUnits.shared.delegate = self
        headerView.dropShadow(shadowColor: Colors.headerViewShadowColor,shadowOpacity: 0.3, shadowOffset: CGSize(width: 0, height: 3), shadowRadius: 1 )
        
        calendarView.scrollToHeaderForDate(Date(), triggerScrollToDateDelegate: true, withAnimation: false, extraAddedOffset: 0, completionHandler: nil)
        calendarView.cellSize = Constants.cellSize
        calendarView.minimumInteritemSpacing = 0
        selectedDatesFromUD = fertilityRanges.selectedPeriodsUD
        calendarViewReloadData(mode: calendarViewModes)
        drawerContentView.delegate = self
        drawerView.attachTo(view: self.view)
        setupDrawer()
        createNotificationCenter()
    }
    
    func upgradeUserUnits() {
        calendarViewReloadData(mode: calendarViewModes)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    func createNotificationCenter(){
        NotificationCenter.default.addObserver(self,selector: #selector(reloadDrawerView), name: NSNotification.Name(rawValue: reloadDetailsNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(reloadUnits), name: NSNotification.Name(rawValue: reloadUnitsNotificationKey), object: nil)
    }
    
    @objc func reloadUnits(){
        calendarViewReloadData(mode: calendarViewModes)
    }
    
    @objc func reloadDrawerView(){
        if selectedDate != nil{
            let rememberSelectedDate = selectedDate!
            calendarView.deselect(dates: [selectedDate!])
            calendarView.selectDates([rememberSelectedDate])
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "calendarGoSymptomsLog"{
            if let nav = segue.destination as? UINavigationController{
                
                let symptomsLogVC = nav.topViewController as! SymptomsLogViewController
                
                symptomsLogVC.date = selectedDate!
            }
        }
    }
    
    func setupDrawer() {
        drawerView.snapPositions = [.closed, .partiallyOpen]
        drawerView.insetAdjustmentBehavior = .automatic
        drawerView.delegate = self
        drawerView.position = .closed
        drawerView.cornerRadius = 20
        drawerView.partiallyOpenHeight = Constants.maxOpenHeight
        drawerView.backgroundColor = .white
        drawerContentView.contentView.backgroundColor = .clear
        drawerView.backgroundEffect  = UIBlurEffect(style: .regular)
    }
    
    func calendarViewReloadData(mode: CalendarViewModes){
        if calendarViewModes == .editingMode{
            editLabel.text = "Done"
            calendarView.allowsMultipleSelection = true
            calendarView.isRangeSelectionUsed = true
            calendarView.reloadData()
            calendarView.selectDates(fertilityRanges.selectedPeriodsUD, triggerSelectionDelegate: false)
        }else{
            editLabel.text = "Edit"
            if selectedDatesFromUD.count != 0{
                 dateInformations = fertilityRanges.dateInformations
                datesInfoDictionary = createDateDictionary(dateInformations: dateInformations)
            }else{
                datesInfoDictionary.removeAll()
            }
        calendarView.deselectAllDates(triggerSelectionDelegate: false)
            calendarView.allowsMultipleSelection = false
            calendarView.isRangeSelectionUsed = false
            calendarView.reloadData()
            NotificationCenter.default.post(name: Notification.Name(rawValue: reloadDateInformationsNotificationKey), object: self)
        }
        
    }
    
    func showDrawerView(customCell: CalendarCell, selectedDate: Date){
        drawerContentView.emojiScrollerView.setupLabels(date: selectedDate, scrollLabels: .emoji)
        var drawerHeight: CGFloat = Constants.minOpenHeight
        drawerContentView.setupLabels(date: selectedDate)
        
        if drawerContentView.emojiScrollerView.isEmojiAdded == false{
            drawerContentView.emojiScrollerView.setupLabels(date: selectedDate, scrollLabels: .title)
            drawerHeight = (drawerContentView.emojiScrollerView.isTitlesAdded == true) ? Constants.medOpenHeight : Constants.minOpenHeight
        }else{
            drawerContentView.titleScrollerView.setupLabels(date: selectedDate, scrollLabels: .title)
            drawerHeight = (drawerContentView.titleScrollerView.isTitlesAdded == true) ? Constants.maxOpenHeight : Constants.medOpenHeight
        }
        if drawerContentView.chanceLabel.text == ""{
            drawerHeight = drawerHeight - 30
        }
        
        drawerView.partiallyOpenHeight = drawerHeight
        drawerView.setPosition(.partiallyOpen, animated: true)
        let cellPositionOnView = customCell.convert(customCell.dateLabel.center , to: self.view)
        var cellPositionOnCalendar = customCell.convert(customCell.center , to: self.view)
        cellPositionOnCalendar.x = 0
        cellPositionOnCalendar.y = cellPositionOnCalendar.y - cellPositionOnView.y - (calendarView.bounds.height - drawerHeight)
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top
        var additionalHeight: CGFloat = 40
        if topPadding! >  CGFloat(20) {
            additionalHeight = 70
        }
        
        if (self.view.bounds.height - cellPositionOnView.y - additionalHeight) < drawerHeight{
            calendarView.setContentOffset(cellPositionOnCalendar,
                                      animated : true)
        }
    }
}

extension CalendarViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        let myCustomCell = cell as! CalendarCell
        if calendarViewModes == .editingMode{
            
           myCustomCell.handleSelectionEditingMode(cellState: cellState, selectedDatesFromUD: selectedDatesFromUD)
        }else{
            myCustomCell.handleSelectionViewingMode(cellState: cellState, datesInfoDictionary: datesInfoDictionary)
        }
    }
   
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "calendarCell", for: indexPath) as! CalendarCell
        cell.dateLabel.text = cellState.text
        cell.roundView.layer.masksToBounds = true
        cell.roundView.layer.cornerRadius = cell.roundView.bounds.height/2
        cell.substrateRound.layer.masksToBounds = true
        cell.substrateRound.layer.cornerRadius = cell.substrateRound.bounds.height/2
        
        if cellState.dateBelongsTo == .thisMonth {
            cell.isHidden = false
        } else {
            cell.isHidden = true
        }
        if calendarViewModes == .editingMode{
            
            cell.handleSelectionEditingMode(cellState: cellState, selectedDatesFromUD: selectedDatesFromUD)
        }else{
            cell.handleSelectionViewingMode(cellState: cellState, datesInfoDictionary: datesInfoDictionary)
        }
        return cell
        
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        let startDate = addMonthComponent(monthComponent: -Constants.nineMonths, toDay: today!)
        let endDate = addMonthComponent(monthComponent: Constants.nineMonths, toDay: today!)

        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfRow, firstDayOfWeek: .sunday, hasStrictBoundaries: nil)
        
        return parameters
    }
 
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        let myCustomCell = cell as! CalendarCell
        if calendarViewModes == .editingMode{
            if myCustomCell.checkImage.isHidden == false{
                selectedDatesFromUD.append(date)
                myCustomCell.handleSelectionEditingMode(cellState: cellState, selectedDatesFromUD: selectedDatesFromUD)
                calendarView.reloadDates([addDayComponent(dayComponent: 1, toDay: cellState.date)])
            }else{
                calendarView.deselect(dates: [cellState.date])
            }
        }else{
            showDrawerView(customCell: myCustomCell, selectedDate: cellState.date)
            
            if selectedDate != nil {
                if selectedDate == cellState.date{
                    calendarView.deselect(dates: [cellState.date])
                    selectedDate = nil
                }else{
                    myCustomCell.handleSelectionViewingMode(cellState: cellState, datesInfoDictionary: datesInfoDictionary)

                    selectedDate = cellState.date
                }
            }else{
                myCustomCell.handleSelectionViewingMode(cellState: cellState, datesInfoDictionary: datesInfoDictionary)

                selectedDate = cellState.date
            }
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        if cell != nil{
            let myCustomCell = cell as! CalendarCell
            if calendarViewModes == .editingMode{
                if myCustomCell.checkImage.isHidden == false{
                    selectedDatesFromUD.remove(at: selectedDatesFromUD.index(of: date)!)
                    
                    myCustomCell.handleSelectionEditingMode(cellState: cellState, selectedDatesFromUD: selectedDatesFromUD)
                    
                    if today! < cellState.date{
                        calendarView.reloadData()
                    }
                }
            }else{
                selectedDate = nil
                drawerView.setPosition(.closed, animated: true)
                
                if cell != nil{
                    myCustomCell.handleSelectionViewingMode(cellState: cellState, datesInfoDictionary: datesInfoDictionary)
                }
            }
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
        let date = range.start
        let header: JTAppleCollectionReusableView
        header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "CalendarHeader", for: indexPath)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM YYYY"
        var month = Formatters.monthYear.string(from: date)
        if Formatters.year.string(from: date) == Formatters.year.string(from: Date()){
            month = Formatters.month.string(from: date)
        }
        (header as! CalendarCollectionReusableView).monthLabel.text = month
        
        return header
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return MonthSize(defaultSize: 50)
    }
}

extension CalendarViewController: DrawerContentViewDelegate {
    
    func addDetails() {
        
        self.performSegue(withIdentifier: "calendarGoSymptomsLog", sender: nil)
    }
    
    func closeDrawerView() {
        drawerView.setPosition(.closed, animated: true)
    }
}

extension CalendarViewController: DrawerViewDelegate {
    
    func drawer(_ drawerView: DrawerView, willTransitionFrom startPosition: DrawerPosition, to targetPosition: DrawerPosition) {
     
        if startPosition == .partiallyOpen {
            if selectedDate != nil{
                calendarView.deselect(dates: [selectedDate!])
            }
        }
    }
}




