import UIKit

class CustomTabBarController:  UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
        self.tabBar.tintColor = Colors.tabBarTintColor
        
    }
}

