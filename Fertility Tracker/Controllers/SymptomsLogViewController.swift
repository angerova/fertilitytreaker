
import UIKit

class SymptomsLogViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var symptomsButtons: [UIButton]!
    @IBOutlet var symptomsLabels: [UILabel]!
    
    var currentSymptomLog : SymptomsLog!
    var date: Date!
    var symptomsLogs: [SymptomsLog] = []
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func switchSymtoms(_ sender: UIButton) {
        currentSymptomLog = symptomsLogs[sender.tag]
        setColorsForButtons(index: sender.tag)
        collectionView.deselectAllItems(animated: false)
        collectionView.reloadData()
        if currentSymptomLog.selectedIndexParths.count != 0{
            collectionView.selectItems(at: currentSymptomLog.selectedIndexParths)
        }
    }
    
    @IBAction func save(_ sender: Any) {
        var allSelectedSymptoms: [SymptomsLogData] = []
        for symptomsLog in symptomsLogs{
            allSelectedSymptoms.append(contentsOf: symptomsLog.getSelectedSymptoms())
        }
        try? daySymptoms.save(allSelectedSymptoms, title: Formatters.date.string(from: date))
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: reloadDetailsNotificationKey), object: self)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createSymptomsLogs()
        currentSymptomLog = symptomsLogs[0]
        setColorsForButtons(index: 0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
        collectionView.reloadData()
        if currentSymptomLog.selectedIndexParths.count != 0{
            collectionView.selectItems(at: currentSymptomLog.selectedIndexParths)
        }
        setupNavigationBar()
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: "AvenirNext-Regular", size: 17)!,
            NSAttributedString.Key.foregroundColor: Colors.navigationBarTitle]
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: "AvenirNext-Regular", size: 17)!,
            NSAttributedString.Key.foregroundColor: Colors.navigationBarTitle], for: .normal)
    }
    
    func createSymptomsLogs(){
        let symptomsLogNames: [SymptomsLogNames] = [.mood, .flow, .intimacy, .symptoms]
        for symptomsLogName in symptomsLogNames{
            let symptomsLog = SymptomsLog(name: symptomsLogName, symptomsLogData: SymptomsLogData.getData(symptomsLog: symptomsLogName), date: date)
            symptomsLogs.append(symptomsLog)
        }
    }
 
    func setColorsForButtons(index: Int){
        for button in symptomsButtons{
            button.alpha = (button.tag == index) ? 1 : 0.4
        }
        for label in symptomsLabels{
            label.textColor = (label.tag == index) ? Colors.activeSymptomLabel : Colors.nonActiveSymptomLabel
        }
    }
}

extension SymptomsLogViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentSymptomLog.symptomsLogData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! SymptomsLogCollectionViewCell
         cell.backgroundColor = (currentSymptomLog.selectedIndexParths.contains(indexPath)) ?  currentSymptomLog.name?.getColor() : Colors.whiteColor
        cell.configure(symptomsLog: currentSymptomLog, indexPath: indexPath)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SymptomsLogCollectionViewCell
            cell.backgroundColor = currentSymptomLog.name?.getColor()
       currentSymptomLog.selectedIndexParths.append(indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SymptomsLogCollectionViewCell
        cell.backgroundColor = .white
        
       currentSymptomLog.selectedIndexParths.remove(at: currentSymptomLog.selectedIndexParths.index(of: indexPath)!)
        
    }

}
extension SymptomsLogViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let spaceBetweenCellAndEdge: CGFloat = CGFloat(43)
        let width = (view.bounds.width - spaceBetweenCellAndEdge)/3

        var height: CGFloat = 0
        switch currentSymptomLog.name{
        case .symptoms?:
            height = width/2
        default:
            height = width
        }
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
}

