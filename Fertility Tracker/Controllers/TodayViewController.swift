

import UIKit

class TodayViewController: UIViewController, UserUnitsDelegate{
    
    @IBOutlet weak var belowDaysLabel: NSLayoutConstraint!
    @IBOutlet weak var aboveDaysLabel: NSLayoutConstraint!
    @IBOutlet weak var addDetailsButton: UIButton!
    @IBOutlet weak var placeHolderImage: UIImageView!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var dayInLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var chanceLabel: UILabel!
    
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var cycleDayLabel: UILabel!
    
    @IBOutlet weak var todayLabelBottom: NSLayoutConstraint!
    @IBOutlet weak var todayLabelTop: NSLayoutConstraint!
    @IBOutlet weak var todayViewTop: NSLayoutConstraint!
    @IBOutlet weak var todayViewWidth: NSLayoutConstraint!
    @IBOutlet weak var todayViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewTop: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    
    @IBOutlet var translucentLeftView: UIView!
    @IBOutlet var translucentRightView: UIView!
    
    @IBOutlet weak var todayView: UIView!
    
    @IBOutlet weak var symptomsScrolView: HorizontalScrollerView!
    
    var constraintWithPH: CGFloat = 0
    var constraintWithoutPH: CGFloat = 0
    var isFirstLoad: Bool = true
    
    @IBAction func addDetails(_ sender: Any) {
        self.performSegue(withIdentifier: "todayGoSymptomsLog", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserUnits.shared.delegate = self
        setupConstrains()
        let title = "TODAY, \(Formatters.day.string(from: Date())) \(Formatters.month.string(from: Date())), \(Formatters.weekDay.string(from: Date()))"
        todayLabel.text = title.uppercased()
        
        setupGradient(view: translucentLeftView, leftColor: Colors.fog.withAlphaComponent(1), middleColor: Colors.fog.withAlphaComponent(0.39), rightColor: Colors.fog.withAlphaComponent(0))
        setupGradient(view: translucentRightView, leftColor: Colors.fog.withAlphaComponent(0), middleColor: Colors.fog.withAlphaComponent(0.39), rightColor: Colors.fog.withAlphaComponent(1))
        addDetailsButton.dropShadow(shadowColor: Colors.addDetailsButtonShadow, shadowOpacity: 0.7, shadowOffset: CGSize(width: 0, height: 0), shadowRadius: addDetailsButton.bounds.height/2 )
        setupLabels()
        createNotificationCenter()
        symptomsScrolView.setupLabelsTogether()
        showScreensFirstTime()
        updateConstrainsForDevice()
        
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isFirstLoad == true{
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let additionalHeight: CGFloat = 40
            if topPadding! >  CGFloat(20) {
                constraintWithPH = constraintWithPH - additionalHeight
            }
            isFirstLoad = false
            showHidePlaceholder()
        }
    }
    
    func createNotificationCenter(){
        NotificationCenter.default.addObserver(self,selector: #selector(reload), name: NSNotification.Name(rawValue: reloadDateInformationsNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(reloadSymptomsScrolView), name: NSNotification.Name(rawValue: reloadDetailsNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(showTabBar), name: NSNotification.Name(rawValue: showTabBarNotificationKey), object: nil)
    }
    
    @objc func reload(){
        setupLabels()
    }

    @objc func reloadSymptomsScrolView(){
        symptomsScrolView.setupLabelsTogether()
        showHidePlaceholder()
    }
    
    @objc func showTabBar(){
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showHidePlaceholder(){
        placeHolderImage.isHidden = (symptomsScrolView.userselectedSymptoms?.count == 0) ? false : true
        scrollViewBottom.constant = (symptomsScrolView.userselectedSymptoms?.count == 0) ? constraintWithPH : constraintWithoutPH
        addDetailsButton.layer.shadowOpacity = (symptomsScrolView.userselectedSymptoms?.count == 0) ? 0.7 : 0
    }
    
    func setupConstrains(){
        let screenWidth = view.bounds.width
        let screenHeight = view.bounds.height
        let roundViewHeight =  screenWidth - 80
        todayViewWidth.constant = roundViewHeight
        todayViewHeight.constant = roundViewHeight
        todayView.layer.masksToBounds = true
        todayView.layer.cornerRadius = roundViewHeight/2
        
        scrollViewTop.constant = (screenHeight - roundViewHeight) * 0.09
        scrollViewBottom.constant = (screenHeight - roundViewHeight) * 0.06
        let constant: CGFloat = (screenHeight == 568) ? 25 : 15
        todayViewTop.constant = (screenHeight - roundViewHeight)/constant
        todayLabelTop.constant = (screenHeight - roundViewHeight)/constant
        todayLabelBottom.constant = (screenHeight - roundViewHeight)/constant
        constraintWithoutPH = (screenHeight - roundViewHeight) * 0.06
        constraintWithPH = ((screenHeight - ((screenHeight - roundViewHeight)/(constant/3) + roundViewHeight - constant + 30)) / 2) * 0.96 - (160)
    }

    func upgradeUserUnits() {
        setupLabels()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "todayGoSymptomsLog"{
            if let nav = segue.destination as? UINavigationController{
                
                let symptomsLogVC = nav.topViewController as! SymptomsLogViewController
                
               symptomsLogVC.date = Date()
            }
        }
    }
    
    func setupLabels(){
        
        let todayDescription = TodayDescription()
        todayView.backgroundColor = todayDescription.backgroundColor
        dayInLabel.text = todayDescription.dayIn
        daysLabel.text = todayDescription.days
        chanceLabel.text = todayDescription.chance
        cycleDayLabel.text = todayDescription.cycleDay
        placeHolderLabel.text = todayDescription.placeHolder
    }
    
    func showScreensFirstTime(){
        let userDefaults = UserDefaults.standard
        var isNotFirstTime = userDefaults.bool(forKey: "isFirstTime")
        
        if isNotFirstTime == false{
            self.tabBarController?.tabBar.isHidden = true
            let startScrollView = StartScreensScrollerView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            view.addSubview(startScrollView)
            isNotFirstTime = true
            userDefaults.set(isNotFirstTime, forKey: "isFirstTime")
        }
    }
}
