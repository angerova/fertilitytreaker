

import UIKit

class ToolsViewController: UIViewController {
    
    @IBOutlet weak var pikerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cycleLengthView: ToolsView!
    @IBOutlet weak var periodLengthView: ToolsView!
    @IBOutlet weak var pickerView: ToolsPickerView!
    
    var currentLengthView: ToolsView? = nil
    var blurEffectView: UIVisualEffectView!
    
    @IBAction func tapPeriodLengthView(_ sender: Any) {
        showPikerView(tappedToolsView: periodLengthView, userNumber: userUnits.periodLength)
    }
    
    @IBAction func tapCycleLengthView(_ sender: Any) {

        showPikerView(tappedToolsView: cycleLengthView, userNumber: userUnits.cycleLength)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        cycleLengthView.dayNumberLabel.text = "\(userUnits.cycleLength) days"
        periodLengthView.dayNumberLabel.text = "\(userUnits.periodLength) days"
        cycleLengthView.nameLabel.text = "Cycle Length"
        periodLengthView.nameLabel.text = "Period Length"
        pikerViewBottom.constant = pickerView.bounds.height
        setupBlurEffect()
        setupShadow()

    }
    
    func setupBlurEffect(){
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.layer.opacity = 0.4
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        blurEffectView.isHidden = true
    }

   private var toolsPickerViewData: [String]!{
        get{
            return (currentLengthView == cycleLengthView) ? ToolsPickerViewData.cycleLength : ToolsPickerViewData.periodLength
        }
    }
    
    
    
    func setupShadow(){
        pickerView.dropShadow(shadowColor: Colors.toolsShadowColor,shadowOpacity: 0, shadowOffset: CGSize(width: 0, height: 0), shadowRadius: 3 )
        cycleLengthView.dropShadow(shadowColor: Colors.toolsShadowColor,shadowOpacity: 0, shadowOffset: CGSize(width: 0, height: 0), shadowRadius: 3 )
        periodLengthView.dropShadow(shadowColor: Colors.toolsShadowColor,shadowOpacity: 0, shadowOffset: CGSize(width: 0, height: 0), shadowRadius: 3 )
    }
    
    func showPikerView(tappedToolsView: ToolsView, userNumber: String ){
        currentLengthView = tappedToolsView
        blurEffectView.isHidden = false
        view.bringSubviewToFront(blurEffectView)
        view.bringSubviewToFront(currentLengthView!)
        view.bringSubviewToFront(pickerView)
        pickerView.reloadToolsPickerView(data: toolsPickerViewData, selectedRow: toolsPickerViewData.index(of: userNumber)!)
        currentLengthView?.layer.shadowOpacity = 0.5
        pickerView?.layer.shadowOpacity = 0.5
        pikerViewBottom.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ToolsViewController: ToolsPickerViewDelegate{
    
    func tools(_ done: Bool) {
        pikerViewBottom.constant = pickerView.bounds.height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        currentLengthView?.layer.shadowOpacity = 0
        pickerView?.layer.shadowOpacity = 0
        blurEffectView.isHidden = true
        currentLengthView = nil
    }
    
    func tools(_ dayNumber: String) {
        currentLengthView?.dayNumberLabel.text = "\(dayNumber) days"
        
        if currentLengthView == cycleLengthView{
            userUnits.cycleLength = dayNumber
        }else{
            userUnits.periodLength = dayNumber
        }
        fertilityRanges.reloadDateInfomations()
        NotificationCenter.default.post(name: Notification.Name(rawValue: reloadUnitsNotificationKey), object: self)
    }
}
