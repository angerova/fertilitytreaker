

import Foundation
import UIKit

typealias Codable = Decodable & Encodable

public class DaySymptoms{
    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()
    private let userDefaults = UserDefaults.standard
    
    public func save(_ daySymptoms: [SymptomsLogData], title: String) throws {
        let data = try encoder.encode(daySymptoms)
        userDefaults.set(data, forKey: title)
    }

    public func load(title: String) throws -> [SymptomsLogData] {
        guard let data = userDefaults.data(forKey: title),
            
            let daySymptoms = try? decoder.decode([SymptomsLogData].self, from: data)
            else {
                
                throw Error.daySymptomsNotFound
        }
        return daySymptoms }
    
    public enum Error: String, Swift.Error {
        case daySymptomsNotFound
    }
    
}

public var daySymptoms = DaySymptoms()
