
import Foundation
import UIKit

extension UIView {
    
    func dropShadow(shadowColor: UIColor,shadowOpacity: Float, shadowOffset: CGSize, shadowRadius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        
    }
}

extension UICollectionView {
    
    func deselectAllItems(animated: Bool) {
        guard let selectedItems = indexPathsForSelectedItems else { return }
        for indexPath in selectedItems { deselectItem(at: indexPath, animated: animated) }
    }
    
    func selectItems(at indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            
        }
    }
}


extension TodayViewController{
    func updateConstrainsForDevice(){
        if view.bounds.height == 568{
            belowDaysLabel.constant = 10
            aboveDaysLabel.constant = 0
            
            updateFontSize()
            
        }
    }
    
    func updateFontSize(){
        chanceLabel.font = UIFont(name: "AvenirNext-Medium", size: 14)
    }
}
