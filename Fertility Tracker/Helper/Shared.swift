

import Foundation
import UIKit



public let reloadDetailsNotificationKey = "reloadDetails.notificationKey"
public let reloadDateInformationsNotificationKey = "reloadDateInformations.notificationKey"
public let reloadUnitsNotificationKey = "reloadUnits.notificationKey"
public let showTabBarNotificationKey = "showTabBar.notificationKey"

public let today = Formatters.date.date(from: Formatters.date.string(from: Date()))

public let userUnits = UserUnits.shared
public let fertilityRanges = FertilityRanges.shared

func getFormatters(format: String)-> DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    return formatter
}

enum Formatters{
    static let date: DateFormatter = getFormatters(format: "yyyy MM dd")
    static let monthDay: DateFormatter = getFormatters(format: "MMM dd")
    static let weekDay: DateFormatter = getFormatters(format: "EEE")
    static let year: DateFormatter = getFormatters(format: "YYYY")
    static let day: DateFormatter = getFormatters(format: "dd")
    static let month: DateFormatter = getFormatters(format: "MMMM")
    static let monthYear: DateFormatter = getFormatters(format: "MMMM YYYY")
}

func addDayComponent(dayComponent: Int, toDay: Date)-> Date{
    var component = DateComponents()
    component.day = dayComponent
    let newDay = Calendar.current.date(byAdding: component, to: toDay)
    return newDay!
}

func addMonthComponent(monthComponent: Int, toDay: Date)-> Date{
    var component = DateComponents()
    component.month = monthComponent
    let newDay = Calendar.current.date(byAdding: component, to: toDay)
    return newDay!
}

enum CalendarViewModes{
    case viewingMode, editingMode
}

enum ScrollLabels{
    case emoji, title
}

func setupGradient(view: UIView, leftColor: UIColor, middleColor: UIColor, rightColor: UIColor){
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [leftColor.cgColor, middleColor.cgColor, rightColor.cgColor ]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
    gradientLayer.frame = view.bounds
    view.layer.addSublayer(gradientLayer)
    
}



enum Colors{
    
    static let intersectionOvulationDay: UIColor = #colorLiteral(red: 0.3314495799, green: 0.3185198244, blue: 0.5571026392, alpha: 1)
    static let intersection: UIColor = #colorLiteral(red: 0.7256569544, green: 0.4454834802, blue: 0.9686274529, alpha: 1)
    static let period: UIColor = #colorLiteral(red: 0.8988142449, green: 0.5532347682, blue: 0.6647359528, alpha: 1)
    static let ovulationPeriod: UIColor = #colorLiteral(red: 0.7757038707, green: 0.905518923, blue: 1, alpha: 1)
    static let ovulationDay: UIColor = #colorLiteral(red: 0.6292283949, green: 0.8041106741, blue: 0.9088866726, alpha: 1)
    static let futureOvulationDay: UIColor = #colorLiteral(red: 0.6096365452, green: 0.8307940364, blue: 0.9815029502, alpha: 1)
    static let selectedEditing: UIColor = #colorLiteral(red: 0.9411764741, green: 0.8730611381, blue: 0.4377266111, alpha: 1)
    static let futurePeriod: UIColor = #colorLiteral(red: 1, green: 0.8676275075, blue: 0.9043083874, alpha: 1)
    static let futureOvulation: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let defaultPeriod: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let late: UIColor = #colorLiteral(red: 0.8735995625, green: 0.8735995625, blue: 0.8735995625, alpha: 1)
    static let whiteColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let blackColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let textPeriod: UIColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
    static let textOvulation: UIColor = #colorLiteral(red: 0.3125533408, green: 0.6545043549, blue: 0.8446057421, alpha: 1)
    static let today: UIColor = #colorLiteral(red: 0.3062319118, green: 0.3062319118, blue: 0.3062319118, alpha: 1)
    static let selectViewing: UIColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
    
     static let fog: UIColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
    
    static let symptomLabelGreen: UIColor = #colorLiteral(red: 0.3764705882, green: 0.862745098, blue: 0.5607843137, alpha: 1)
    static let symptomLabelYellow: UIColor = #colorLiteral(red: 1, green: 0.8784313725, blue: 0.3294117647, alpha: 1)
    
    static let navigationBarTitle: UIColor = #colorLiteral(red: 0.3450980392, green: 0.3450980392, blue: 0.3450980392, alpha: 1)
    
    static let doneImageViewTintColor: UIColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    static let pickerViewForegroundColor: UIColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    
    static let addDetailsButton: UIColor = #colorLiteral(red: 0.4324534535, green: 0.8764716983, blue: 0.6284565926, alpha: 1)
    static let addDetailsButtonShadow: UIColor = #colorLiteral(red: 0.4352941176, green: 0.9294117647, blue: 0.6196078431, alpha: 1)
    
    static let headerViewShadowColor: UIColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    
    static let activeSymptomLabel: UIColor = #colorLiteral(red: 0.3450980392, green: 0.3450980392, blue: 0.3450980392, alpha: 1)
    static let nonActiveSymptomLabel: UIColor = #colorLiteral(red: 0.662745098, green: 0.662745098, blue: 0.662745098, alpha: 1)
    static let tabBarTintColor: UIColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1)

    static let toolsShadowColor: UIColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    
    static let selectedMood: UIColor = #colorLiteral(red: 0.9921568627, green: 1, blue: 0.9176470588, alpha: 1)
    static let selectedSymptoms: UIColor = #colorLiteral(red: 0.9058823529, green: 1, blue: 0.9411764706, alpha: 1)
    static let selectedFlow: UIColor = #colorLiteral(red: 0.9137254902, green: 0.968627451, blue: 1, alpha: 1)
    static let selectedIntimacy: UIColor = #colorLiteral(red: 1, green: 0.9450980392, blue: 0.9725490196, alpha: 1)
    
  
    static let todayPeriod: UIColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    static let todayOvulation: UIColor = #colorLiteral(red: 0.1254901961, green: 0.6980392157, blue: 1, alpha: 1)
    static let todayDefault: UIColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
    static let todayHolder: UIColor = #colorLiteral(red: 0.7424106671, green: 0.5167903702, blue: 1, alpha: 1)
    static let todayLate: UIColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    
    static let nextButton: UIColor = #colorLiteral(red: 0.4641033411, green: 0.5728133321, blue: 0.9409721494, alpha: 1)
    static let saveButton: UIColor = #colorLiteral(red: 0.9361248612, green: 0.4627882838, blue: 0.6780118942, alpha: 1)
    
}

enum Constants{
    
    static let minMonthForCalculation = 3
    static let minDaysInCycle = 20
    static let maxDaysInCycle = 56
    static let daysBeforeOvulation = -4
    static let daysAfterOvulation = 2
    static let daysToOvulationDay = -14
    static let nineMonths = 9
    static let cellSize: CGFloat = 65
    static let minOpenHeight: CGFloat = 230
    static let maxOpenHeight: CGFloat = 400
    static let medOpenHeight: CGFloat = 320
}


enum ToolsPickerViewData{
    static let cycleLength: [String] = Array(21...56).map { String($0)}
    static let periodLength: [String] = Array(1...14).map { String($0)}
}
