

import Foundation
import UIKit
import JTAppleCalendar

public struct CellColorsPosition{
    var left: Bool
    var right: Bool
    var middle: Bool
    
    init(cellPosition: SelectionRangePosition){
        left = true
        right = true
        middle = true
        switch cellPosition {
            
        case .full:
            left = false
            right = false
            middle = true
        case .left:
            left = true
            right = false
            middle = true
            
            
        case .right:
            left = false
            right = true
            middle = true
            
        case .middle:
            left = true
            right = true
            middle = true
        default:
            left = false
            right = false
            middle = false
        }
    }
}

public struct CellColorsAndImageEditingMode{
    var leftView: UIColor
    var rightView: UIColor
    var roundView: UIImage
    init(cellColorsPosition: CellColorsPosition, colorTrue: UIColor, colorFalse: UIColor, imageTrue: UIImage, imageFalse: UIImage){
        leftView = (cellColorsPosition.left == true) ? colorTrue : colorFalse
        rightView = (cellColorsPosition.right == true) ? colorTrue : colorFalse
        roundView = (cellColorsPosition.middle == true) ? imageTrue : imageFalse        
    }
}

public struct CellColorsViewingMode{
    var leftView: UIColor
    var rightView: UIColor
    var roundView: UIColor
    var text: UIColor
    
    init(datesInfoDictionary: [String:DateInformation], position: SelectionRangePosition, date: Date) {
        let dayAfter = datesInfoDictionary[Formatters.date.string(from: addDayComponent(dayComponent: 1, toDay: date)) ]
        let dateInformation = datesInfoDictionary[Formatters.date.string(from: date)]
        
        switch position {
        case .left:
            if Formatters.day.string(from: date) == "01"{
                rightView = .white
                leftView = (dayAfter?.rangeType == .intersection || dayAfter?.rangeType == .intersectionOvulationDay)  ? Colors.intersection : Colors.ovulationPeriod
                roundView = (dateInformation?.rangeType == .intersection) ? Colors.intersection : Colors.intersectionOvulationDay
                text = .white
            }else{
               rightView = Colors.period
               leftView = (dayAfter?.rangeType == .intersection || dayAfter?.rangeType == .intersectionOvulationDay)  ? Colors.intersection : Colors.ovulationPeriod
                roundView = (dateInformation?.rangeType == .intersection) ? Colors.intersection : Colors.intersectionOvulationDay
                text = .white
            }
        case .right:
            rightView = Colors.intersection
            leftView = .white
            roundView = (dateInformation?.rangeType == .intersection) ? Colors.intersection : Colors.intersectionOvulationDay
            text = .white
            
        case .middle:
            rightView  = Colors.intersection
            leftView = (dayAfter?.rangeType == .intersection || dayAfter?.rangeType == .intersectionOvulationDay)  ? Colors.intersection : Colors.ovulationPeriod

            roundView = (dateInformation?.rangeType == .intersection) ? Colors.intersection : Colors.intersectionOvulationDay
            text = .white
            
        case .full:
            
            rightView = Colors.period
            leftView = .white
            roundView = (dateInformation?.rangeType == .intersection) ? Colors.intersection : Colors.intersectionOvulationDay
            text = .white
        default:
            leftView = .white
            rightView = .white
            roundView = .white
            text = .black
        }
    }
    
    init(cellColorsPosition: CellColorsPosition, rangeType: RangeType){
        switch rangeType {
    
        case .period:
            leftView = (cellColorsPosition.left == true) ? Colors.period : .white
            rightView = (cellColorsPosition.right == true) ? Colors.period : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.period : .white
            text = Colors.whiteColor
        case .ovulationPeriod:
            leftView = (cellColorsPosition.left == true) ? Colors.ovulationPeriod : .white
            rightView = (cellColorsPosition.right == true) ? Colors.ovulationPeriod : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.ovulationPeriod : .white
            text = Colors.blackColor
        case .futurePeriod:
            leftView = (cellColorsPosition.left == true) ? Colors.futurePeriod : .white
            rightView = (cellColorsPosition.right == true) ? Colors.futurePeriod : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.futurePeriod : .white
            text = Colors.textPeriod
        case .futureIntersection:
            leftView = (cellColorsPosition.left == true) ? Colors.futurePeriod : .white
            rightView = (cellColorsPosition.right == true) ? Colors.futurePeriod : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.futurePeriod : .white
            text = Colors.textPeriod
        case .futureOvulation:
            leftView = (cellColorsPosition.left == true) ? Colors.futureOvulation : .white
            rightView = (cellColorsPosition.right == true) ? Colors.futureOvulation : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.futureOvulation : .white
            text = Colors.textOvulation
        case .ovulationDay:
            leftView = (cellColorsPosition.left == true) ? Colors.ovulationPeriod : .white
            rightView = (cellColorsPosition.right == true) ? Colors.ovulationPeriod : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.ovulationDay : .white
            text = Colors.whiteColor
        case .futureOvulationDay:
            leftView = (cellColorsPosition.left == true) ? Colors.futureOvulation : .white
            rightView = (cellColorsPosition.right == true) ? Colors.futureOvulation : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.futureOvulationDay : .white
            text = Colors.textOvulation
        case .late:
            leftView = (cellColorsPosition.left == true) ? Colors.late : .white
            rightView = (cellColorsPosition.right == true) ? Colors.late : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.late : .white
            text = Colors.blackColor
        
        default:
            leftView = (cellColorsPosition.left == true) ? Colors.period : .white
            rightView = (cellColorsPosition.right == true) ? Colors.period : .white
            roundView = (cellColorsPosition.middle == true) ? Colors.period : .white
            text = Colors.blackColor
        }
    }
}

func checkIsFirstAndLastMonthDay(date: Date, in Dates: [Date])-> SelectionRangePosition? {
    var position: SelectionRangePosition? = nil
    let dayBefore = addDayComponent(dayComponent: -1, toDay: date)
    let dayAfter = addDayComponent(dayComponent: 1, toDay: date)
    if Formatters.day.string(from: date) == "01"{
        position = (Dates.contains(dayAfter)) ? .left : .full
        
    }else if Formatters.day.string(from: dayAfter) == "01"{
        position = (Dates.contains(dayBefore)) ? .right : .full
    }
    return position
}


