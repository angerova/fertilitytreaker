

import Foundation
import UIKit

class DayDescription {
    
    var dateString: String = ""
    var chance: String = ""
    var cycleDay: String = ""
    
    init(date: Date){
        var dateString = ""
        var chance = ""
        var cycleDay = ""
        
        let dateInformations = fertilityRanges.dateInformations.sorted(by: { $0.date < $1.date })
        dateString = setupDateString(selectedDate: date)
        chance = setupChance(dateInformations: dateInformations, selectedDate: date)
        cycleDay = setupCycleDay(dateInformations: dateInformations, selectedDate: date)
        
        self.dateString = dateString
        self.chance = chance
        self.cycleDay = cycleDay
    }
    
    private func setupDateString(selectedDate: Date)-> String{
        
        
        let todayString = (today == selectedDate) ? "Today, " : ""
        
        let dateString = "\(todayString)\(Formatters.day.string(from: selectedDate)) \(Formatters.month.string(from: selectedDate)), \(Formatters.weekDay.string(from: selectedDate))"
        return dateString
    }
    
    
    private func setupChance(dateInformations: [DateInformation], selectedDate: Date)-> String{
        var chance = ""
        if dateInformations.count != 0{
            let firstSelectedDate = dateInformations[0].date
            if firstSelectedDate <= selectedDate{
                var rangeType: RangeType = RangeType.defaultPeriod
                for dateInformation in dateInformations{
                    if dateInformation.date == selectedDate{
                        rangeType = dateInformation.rangeType
                        break
                    }
                }
                
                
                switch rangeType{
                    
                    
                case .period, .futurePeriod,.late, .defaultPeriod:
                    chance = Chances.low
                    
                    
                case .ovulationPeriod, .futureOvulation, .futureIntersection, .intersection:
                    chance = Chances.normal
                    
                case .ovulationDay, .futureOvulationDay, .intersectionOvulationDay:
                    chance = Chances.high
                }

            }
            
        }
        
        return chance
    }
    
    private func setupCycleDay(dateInformations: [DateInformation], selectedDate: Date) -> String{
        var cycleDay = ""
        if dateInformations.count != 0{
            
            var startDateOfCycle = dateInformations[0].date
            var previousDI = dateInformations[0]
            for dateInformation in dateInformations{
                if dateInformation.rangeType == .period || dateInformation.rangeType == .futurePeriod{
                   
                    if previousDI.date != addDayComponent(dayComponent: -1, toDay: dateInformation.date){
                        
                        
                        if selectedDate >= dateInformation.date{
                            startDateOfCycle = dateInformation.date
                           
                            
                        }else{
                            
                            break
                        }
                    }
                }
                previousDI = dateInformation
            }
            
            let dayOfCycle = (Calendar.current.dateComponents([.day], from: startDateOfCycle, to: selectedDate).day ?? 0) + 1
            if dayOfCycle > 0 {
                cycleDay = "\(dayOfCycle)th day of cycle"
            }else{
                cycleDay = ""
            }
        }else{
            cycleDay = ""
        }
        
        return cycleDay
        
    }

    private enum Chances{
        static let low: String = "Low chance of getting pregnant"
        static let normal: String = "Chance to get pregnant"
        static let high: String = "High chance of getting pregnant"
    }
    
    
}
