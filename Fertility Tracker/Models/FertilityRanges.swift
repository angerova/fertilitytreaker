

import Foundation
import UIKit
import JTAppleCalendar

enum RangeType{
    case period, ovulationDay, ovulationPeriod, futureOvulation, futurePeriod, futureOvulationDay, defaultPeriod, intersection, intersectionOvulationDay, futureIntersection, late
}

public struct DateInformation{
    var date: Date
    var position: SelectionRangePosition
    var rangeType: RangeType
}

public struct LastCycle{
    var cycleLength: Int? = nil
    var startDay: Date? = nil
    var startDayOfNextCycle: Date? = nil
    
    init(selectedDates: [Date]){
        if selectedDates.count != 0{
            let sortedSelectedDates = selectedDates.sorted(by: { $0 > $1 })
            var count = 0
            var cyclesSum = 0
            var startDay = sortedSelectedDates.last
            var startDaysOfCycle: [Date] = []
            for date in sortedSelectedDates{
                let isDayBefore = sortedSelectedDates.contains(addDayComponent(dayComponent: -1, toDay: date))
                let diffInMonth = Calendar.current.dateComponents([.month], from: date, to: Date()).month

                
                if diffInMonth! > Constants.minMonthForCalculation{
                
                    break
                    
                }
                if isDayBefore == false{
                    
                    
                    if date != sortedSelectedDates.last{
                        let nextDateInAllDates = startDaysOfCycle.last
                        let diffInDays = Calendar.current.dateComponents([.day], from: date, to: nextDateInAllDates ?? date).day

                        if diffInDays! > Constants.minDaysInCycle , diffInDays! < Constants.maxDaysInCycle{
                            count = count + 1
                            cyclesSum = cyclesSum + diffInDays!
                        }
                    }
                    startDaysOfCycle.append(date)
                }
                if count == Constants.minMonthForCalculation{
                   
                    break
                }
            }
            var cycleLength = Int(userUnits.cycleLength)
        
            if count >= Constants.minMonthForCalculation {
                cycleLength = Int(cyclesSum/count)
            }
            if startDaysOfCycle.count != 0 {
                startDay = startDaysOfCycle[0]
            }
            self.startDay = startDay
            self.cycleLength = cycleLength
            self.startDayOfNextCycle =  addDayComponent(dayComponent: cycleLength!, toDay: startDay!)
        }
    }
}

public class FertilityRanges{
    private struct Keys {
        static let selectedPeriodsKey = "SavedArray"
    }
    
    public var selectedPeriodsUD: [Date]{
        get{
            
            return UserDefaults.standard.array(forKey: Keys.selectedPeriodsKey) as? [Date] ?? []
        }
        set{
            UserDefaults.standard.set(newValue, forKey: Keys.selectedPeriodsKey)
        }
    }
    
    public var dateInformations: [DateInformation] = []
    
    public static let shared = FertilityRanges()
    
    private init(){
        reloadDateInfomations()
    }
  
    func reloadDateInfomations(){
        removeOldSelectedDates()
        var dateInformations: [DateInformation] = []
        let selectedDates = self.selectedPeriodsUD
        if selectedDates.count != 0{
            let sortedSelectedDates = selectedDates.sorted(by: { $0 > $1 })
           
            let lastCycle = LastCycle(selectedDates: sortedSelectedDates)
            
            
            dateInformations = createDateInformationsForPeriod(selectedDates: sortedSelectedDates.sorted(), period: .period, ovulationPeriod: .ovulationPeriod, ovulationPeriodDay: .ovulationDay)
            let ovulitionDay = addDayComponent(dayComponent: Constants.daysToOvulationDay, toDay: lastCycle.startDayOfNextCycle!)
            let ovulationPeriod: RangeType = .ovulationPeriod
            let ovulationPeriodDay: RangeType = .ovulationDay
            for i in Constants.daysBeforeOvulation...Constants.daysAfterOvulation{
                
                    
                    if selectedDates.contains(addDayComponent(dayComponent: i, toDay: ovulitionDay)){
                        var newOvulationPeriod = ovulationPeriod
                        newOvulationPeriod = (ovulationPeriod == .ovulationPeriod) ? .intersection : .futureIntersection
                        var dateInformation = dateInformationForOvulation(index: i, date: addDayComponent(dayComponent: i, toDay: ovulitionDay), ovulationPeriod: newOvulationPeriod, ovulationPeriodDay: ovulationPeriodDay)
                        if newOvulationPeriod == .futureIntersection{
                            dateInformation.position = setDatePosition(date: addDayComponent(dayComponent: i, toDay: ovulitionDay), in: selectedDates)
                            
                        }
                        dateInformations.append(dateInformation)
                        
                        
                    }else{
                        dateInformations.append(dateInformationForOvulation(index: i, date: addDayComponent(dayComponent: i, toDay: ovulitionDay), ovulationPeriod: ovulationPeriod, ovulationPeriodDay: ovulationPeriodDay))
                    }
                    
                    
                
//                dateInformations.append(dateInformationForOvulation(index: i, date: addDayComponent(dayComponent: i, toDay: addDayComponent(dayComponent: Constants.daysToOvulationDay, toDay: lastCycle.startDayOfNextCycle!)), ovulationPeriod: .ovulationPeriod, ovulationPeriodDay: .ovulationDay))
            }
            
            if today! <= lastCycle.startDayOfNextCycle!{
                var addedDateInformations: [DateInformation] = []
                let predictedDays = addPredictedDates(startDayOfPredictedCycle: lastCycle.startDayOfNextCycle!, cycle: lastCycle.cycleLength!)
                addedDateInformations = createDateInformationsForPeriod(selectedDates: predictedDays, period: .futurePeriod, ovulationPeriod: .futureOvulation, ovulationPeriodDay: .futureOvulationDay)
                dateInformations.append(contentsOf: addedDateInformations)
                
            }else{
                dateInformations.append(contentsOf: createDateInformationsForLate(startLateDay: lastCycle.startDayOfNextCycle!, selectedDates: selectedDates))
                
            }

        }
        
        
        
        self.dateInformations = dateInformations
    }
    
    private func createDateInformationsForLate(startLateDay: Date, selectedDates:[Date])-> [DateInformation]{
        var dateInformations: [DateInformation] = []
        var i = startLateDay
        
        var lateDates: [Date] = []
        while i <= today! {
            lateDates.append(i)
            i = addDayComponent(dayComponent: 1, toDay: i)
            
        }
        for lateDate in lateDates{
            let position = setDatePosition(date: lateDate, in: lateDates)
            let dateInformation = DateInformation(date: lateDate, position: position, rangeType: .late)
            
            dateInformations.append(dateInformation)
            
        }
        
        return dateInformations
    }
    
    
    private func createDateInformationsForPeriod(selectedDates:[Date], period: RangeType, ovulationPeriod: RangeType, ovulationPeriodDay: RangeType )-> [DateInformation]{
        
        var dateInformations: [DateInformation] = []
        
        var previousSelectedDate = selectedDates[0]
        var startDayOfPreviousCycle = selectedDates[0]
        
        
        for date in selectedDates{
            
            
            if previousSelectedDate != addDayComponent(dayComponent: -1, toDay: date){
                let diffInDays = Calendar.current.dateComponents([.day], from: startDayOfPreviousCycle, to: date).day
                if diffInDays! > Constants.minDaysInCycle , diffInDays! < Constants.maxDaysInCycle{
                    let ovulitionDay = addDayComponent(dayComponent: Constants.daysToOvulationDay, toDay: date)
                    for i in Constants.daysBeforeOvulation...Constants.daysAfterOvulation{
                        
                        if selectedDates.contains(addDayComponent(dayComponent: i, toDay: ovulitionDay)){
                            var newOvulationPeriod = ovulationPeriod
                            newOvulationPeriod = (ovulationPeriod == .ovulationPeriod) ? .intersection : .futureIntersection
                            var dateInformation = dateInformationForOvulation(index: i, date: addDayComponent(dayComponent: i, toDay: ovulitionDay), ovulationPeriod: newOvulationPeriod, ovulationPeriodDay: ovulationPeriodDay)
                            if newOvulationPeriod == .futureIntersection{
                                dateInformation.position = setDatePosition(date: addDayComponent(dayComponent: i, toDay: ovulitionDay), in: selectedDates)
                                
                            }
                            dateInformations.append(dateInformation)
                            
                            
                        }else{
                            dateInformations.append(dateInformationForOvulation(index: i, date: addDayComponent(dayComponent: i, toDay: ovulitionDay), ovulationPeriod: ovulationPeriod, ovulationPeriodDay: ovulationPeriodDay))
                        }
                        
                        
                    }
                }
                startDayOfPreviousCycle = date
            }
            previousSelectedDate = date
            
            let position = setDatePosition(date: date, in: selectedDates)
            
            let newDateInformation = DateInformation(date: date, position: position, rangeType: period)
            dateInformations.append(newDateInformation)
            
        }
        
        return dateInformations
    }
    
    private func dateInformationForOvulation(index: Int,date: Date, ovulationPeriod: RangeType, ovulationPeriodDay: RangeType )-> DateInformation{
        
        var rangeType: RangeType = ovulationPeriod
        var position: SelectionRangePosition = .middle
        let day = Formatters.day.string(from: date)
        let dayAfter = Formatters.day.string(from: addDayComponent(dayComponent: 1, toDay: date))
        if index == 0{
            rangeType = (ovulationPeriod == .intersection) ? RangeType.intersectionOvulationDay :  ovulationPeriodDay
            if ovulationPeriod == .futureIntersection{
                rangeType = .futureIntersection
            }
        }
        
        if index == Constants.daysBeforeOvulation{
            position = .left
        }else if index == Constants.daysAfterOvulation{
            position = .right
        }
        if day == "01"{
            if index == Constants.daysAfterOvulation{
                position = .full
            }else{
                position = .left
            }
        }
        if dayAfter == "01"{
            if index != Constants.daysBeforeOvulation {
                position = .right
            }else{
                position = .full
            }
        }
        
        let dateInformation = DateInformation(date: date, position: position, rangeType: rangeType)
        return dateInformation
    }
    
    private func setDatePosition(date: Date, in Dates: [Date])-> SelectionRangePosition{
        let isDayAfter = Dates.contains(addDayComponent(dayComponent: 1, toDay: date))
        let isDayBefore = Dates.contains(addDayComponent(dayComponent: -1, toDay: date))
        var position: SelectionRangePosition = .none
        if isDayAfter == true,isDayBefore == true {
            position = .middle
            
        }else if isDayAfter == false,isDayBefore == true {
            position = .right
        }else if isDayAfter == true,isDayBefore == false{
            position = .left
        }else{
            position = .full
        }
        
        let day = Formatters.day.string(from: date)
        
        let dayAfter = Formatters.day.string(from: addDayComponent(dayComponent: 1, toDay: date))
        if day == "01"{
            if isDayAfter == false{
                position = .full
            }else{
                position = .left
            }
        }
        if dayAfter == "01"{
            if isDayBefore == true {
                
                position = .right
            }else{
                position = .full
            }
        }
        
        return position
    }
    
   private func addPredictedDates(startDayOfPredictedCycle: Date, cycle: Int)-> [Date]{
        
        var predictedDates: [Date] = []
        var starDate = startDayOfPredictedCycle
        
        for _ in 0...Constants.nineMonths{
            predictedDates.append(starDate)
            if Int(userUnits.periodLength)! > 1{
                for i in 1...(Int(userUnits.periodLength)! - 1){
                    predictedDates.append(addDayComponent(dayComponent: i, toDay: starDate))
                }
            }
            
            starDate = addDayComponent(dayComponent: cycle, toDay: starDate)
        }
        return predictedDates
    }
    
    private func removeOldSelectedDates(){
        var selectedDates = self.selectedPeriodsUD
        if selectedDates.count != 0{
            selectedDates.sort()
            
            
            let startDate = addMonthComponent(monthComponent: -(Constants.nineMonths - 1), toDay: today!)
            var index = -1
            for date in selectedDates{
                if date < startDate{
                    index = index + 1
                }else{
                    break
                }
            }
            if index >= 0{
                for _ in 0...index{
                    selectedDates.remove(at: 0)
                }
            }
            self.selectedPeriodsUD = selectedDates
        }
    }
        

}



func createDateDictionary(dateInformations: [DateInformation])-> [String:DateInformation] {
    var dictionary =  [String:DateInformation]()
    
    for dateInformation in dateInformations{
        let dateString = Formatters.date.string(from: dateInformation.date)
        dictionary.updateValue(dateInformation, forKey: dateString)
    }
    return dictionary
}





