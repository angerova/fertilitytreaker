

import Foundation
import UIKit


public enum SymptomsLogNames {
    case mood, symptoms, flow, intimacy
    
    func getColor()-> UIColor{
        
        var color = Colors.whiteColor
        switch self {
        case .mood:
            color = Colors.selectedMood
        case .symptoms:
            color = Colors.selectedSymptoms
        case .flow:
            color = Colors.selectedFlow
        case .intimacy:
            color = Colors.selectedIntimacy
        
        }
        return color
    }
}

class SymptomsLog{
    var name: SymptomsLogNames? = nil
    var selectedIndexParths: [IndexPath] = []
    var symptomsLogData: [SymptomsLogData] = []
    
    init(name: SymptomsLogNames,symptomsLogData: [SymptomsLogData], date: Date) {
        self.name = name
        self.symptomsLogData = symptomsLogData
        let  userselectedSymptoms = try? daySymptoms.load(title: Formatters.date.string(from: date))
        
        if userselectedSymptoms != nil{
            for index in 0...(symptomsLogData.count-1){
                let sumptom = symptomsLogData[index]
                if userselectedSymptoms!.contains(where:{ $0.title == sumptom.title}){
                    let indexPath = IndexPath(item: index, section: 0)
                    selectedIndexParths.append(indexPath)
                }
            }
        }
    }
    
    func getSelectedSymptoms()-> [SymptomsLogData]{
        var selectedSymptoms:[SymptomsLogData] = []
        for selectedIndexParth in selectedIndexParths{            selectedSymptoms.append(symptomsLogData[selectedIndexParth.row])
        }
        return selectedSymptoms
    }  
}


