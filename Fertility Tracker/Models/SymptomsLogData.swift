

import Foundation
import UIKit

public struct SymptomsLogData: Codable{
    let emoji: String?
    let title: String
}

extension SymptomsLogData {
   public static func getData(symptomsLog: SymptomsLogNames)->[SymptomsLogData]{
        var data: [SymptomsLogData] = []
        
        switch symptomsLog{
            
        case .mood:
           data = [SymptomsLogData(emoji: "😇", title: "Calm"),SymptomsLogData(emoji: "☺️", title: "Happy"),SymptomsLogData(emoji: "🙃", title: "Energetic"),SymptomsLogData(emoji: "😉", title: "Frisky"),SymptomsLogData(emoji: "😏", title: "Swings"),SymptomsLogData(emoji: "😤", title: "Irritated"),SymptomsLogData(emoji: "😢", title: "Sad"),SymptomsLogData(emoji: "😟", title: "Anxious"),SymptomsLogData(emoji: "😥", title: "Depressed"),SymptomsLogData(emoji: "😒", title: "Apathetic"),SymptomsLogData(emoji: "😔", title: "Melancholy"),SymptomsLogData(emoji: "😐", title: "Neutral")]
        case .flow:
            data = [SymptomsLogData(emoji: "💧", title: "Light"),SymptomsLogData(emoji: "💦", title: "Medium"),SymptomsLogData(emoji: "☔️", title: "Heavy")]
        case .symptoms:
            data = [SymptomsLogData(emoji: nil, title: "All fine"),SymptomsLogData(emoji: nil, title: "Cramps"),SymptomsLogData(emoji: nil, title: "Acne"),SymptomsLogData(emoji: nil, title: "Tender \nbreasts"),SymptomsLogData(emoji: nil, title: "Headache"),SymptomsLogData(emoji: nil, title: "Constipation"),SymptomsLogData(emoji: nil, title: "Backache"),SymptomsLogData(emoji: nil, title: "Nausea"),SymptomsLogData(emoji: nil, title: "Bloating"),SymptomsLogData(emoji: nil, title: "Fatigue"),SymptomsLogData(emoji: nil, title: "Cravings"),SymptomsLogData(emoji: nil, title: "Insomnia"),SymptomsLogData(emoji: nil, title: "Diarrhea"),SymptomsLogData(emoji: nil, title: "Chills"),SymptomsLogData(emoji: nil, title: "Muscle \npain")]
        case .intimacy:
            data = [SymptomsLogData(emoji: "🌵", title: "No Sex"),SymptomsLogData(emoji: "💝", title: "Protected sex"),SymptomsLogData(emoji: "💖", title: "Unprotected"),SymptomsLogData(emoji: "🐇", title: "High drive"),SymptomsLogData(emoji: "💘", title: "Orgasm"),SymptomsLogData(emoji: "💛", title: "Masturbation")]
        }
        return data
    }
}





