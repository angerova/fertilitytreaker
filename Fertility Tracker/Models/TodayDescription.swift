

import Foundation
import UIKit


class TodayDescription: DayDescription {
    var backgroundColor: UIColor = Colors.whiteColor
    var dayIn: String = ""
    var days: String = ""
    var placeHolder: String = ""
    
    init() {
        
        super.init(date: today!)
        let dateInformations = fertilityRanges.dateInformations
        if dateInformations.count > 0 {
            var rangeType: RangeType = RangeType.defaultPeriod
            for dateInformation in dateInformations{
                if dateInformation.date == today{
                    rangeType = dateInformation.rangeType
                    break
                }
            }
            
            let lastCycle = LastCycle(selectedDates: fertilityRanges.selectedPeriodsUD)
            let dayOfCycle = (Calendar.current.dateComponents([.day], from: lastCycle.startDay!, to: today!).day ?? 0) + 1
            
            switch rangeType{
                
            case .period,.intersection:
                self.backgroundColor = Colors.todayPeriod
                self.dayIn = "Period"
                
                self.days = "Day \(dayOfCycle)"
                
            case .ovulationDay:
                self.backgroundColor = Colors.todayOvulation
                self.dayIn = "Prediction: Day of"
                self.days = "Ovulation"
            
            case .ovulationPeriod:
                let ovulationDay = addDayComponent(dayComponent: -14, toDay: lastCycle.startDayOfNextCycle!)
                if today! > ovulationDay{
                    self.backgroundColor = Colors.todayOvulation
                    self.dayIn = "Period in"
                    self.days = "\(Calendar.current.dateComponents([.day], from: today!, to: lastCycle.startDayOfNextCycle!).day!) days"
                    
                }
                if today! < ovulationDay{
                    self.backgroundColor = Colors.todayOvulation
                    self.dayIn = "Ovulation in"
                    let plural = (Calendar.current.dateComponents([.day], from: today!, to: ovulationDay).day! == 1) ? "" : "s"
                    
                    self.days = "\(Calendar.current.dateComponents([.day], from: today!, to: ovulationDay).day!) day\(plural)"
                }
                
            case .futurePeriod:
                self.backgroundColor = Colors.todayPeriod
                self.dayIn = "Prediction: Period"
                self.days = "Day \(1)"
                
                
            case .late:
                self.backgroundColor = Colors.todayLate
                self.dayIn = "Late for"
                
                self.days = "\(Calendar.current.dateComponents([.day], from: lastCycle.startDayOfNextCycle!, to: today!)) days"
                self.days = "\(Calendar.current.dateComponents([.day], from: lastCycle.startDayOfNextCycle!, to: today!).day!) days"
                
                
                
            case .defaultPeriod:
                let ovulationDay = addDayComponent(dayComponent: -14, toDay: lastCycle.startDayOfNextCycle!)
                if today! < ovulationDay{
                    let plural = (Calendar.current.dateComponents([.day], from: today!, to: ovulationDay).day! == 1) ? "" : "s"
                    backgroundColor = Colors.todayDefault
                    dayIn = "Ovulation in"
                    days = "\(Calendar.current.dateComponents([.day], from: today!, to: ovulationDay).day!) day\(plural)"
                }
                if today! > ovulationDay{
                    let plural = (Calendar.current.dateComponents([.day], from: today!, to: lastCycle.startDayOfNextCycle!).day! == 1) ? "" : "s"
                    self.backgroundColor = Colors.todayDefault
                    self.dayIn = "Period in"
                    self.days = "\(Calendar.current.dateComponents([.day], from: today!, to: lastCycle.startDayOfNextCycle!).day!) day\(plural)"
                    
                }
                if today! > lastCycle.startDayOfNextCycle!{
                    
                    backgroundColor = Colors.todayLate
                    dayIn = "Late for"
                    days = "\((Calendar.current.dateComponents([.day], from: lastCycle.startDayOfNextCycle! , to: today!).day!) + 1) Day"
                   
                }
                if today! < lastCycle.startDay!{
                    
                    let plural = (Calendar.current.dateComponents([.day], from: today!, to: lastCycle.startDay!).day! == 1) ? "" : "s"
                    self.backgroundColor = Colors.todayDefault
                    self.dayIn = "Period in"
                    self.days = "\(Calendar.current.dateComponents([.day], from: today!, to: lastCycle.startDay!).day!) day\(plural)"
                }
            default:
                self.backgroundColor = Colors.whiteColor
                self.dayIn = ""
                self.days = ""
            }
        }else{
            self.backgroundColor = Colors.todayHolder
            self.placeHolder = "Predictions will be more accurate if you log your past periods"
        }
    }
}

