//
//  UserDataInput.swift
//  Fertility Tracker
//
//  Created by Алина Ангерова on 29/05/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//



import Foundation
import UIKit


public enum StartScreens {
    case lastDayScreen, cycleLengthScreen, periodLengthScreen
    
}

public struct UserDataScreen{
    public let name: StartScreens
    public let title: String
    public let buttonColor: UIColor
    public let buttonText: String
    public let isDatePicker: Bool
    public let pickerData: [String]?
}

extension UserDataScreen{

    public static func allScreensData() -> [UserDataScreen] {
        let lastDayScreen = UserDataScreen(name: StartScreens.lastDayScreen, title: "Choose day your last period started", buttonColor: Colors.nextButton, buttonText: "Next", isDatePicker: true, pickerData: nil)
        let cycleLengthScreen = UserDataScreen(name: StartScreens.cycleLengthScreen, title: "How many days on average is your cycle?", buttonColor: Colors.nextButton, buttonText: "Next", isDatePicker: false, pickerData: ToolsPickerViewData.cycleLength)
        let periodLengthScreen = UserDataScreen(name: StartScreens.periodLengthScreen, title: "How many days on average is your menstruation?", buttonColor: Colors.saveButton, buttonText: "Save", isDatePicker: false, pickerData: ToolsPickerViewData.periodLength)
        return [lastDayScreen, cycleLengthScreen, periodLengthScreen]

    }
}


