
import Foundation

protocol UserUnitsDelegate: class {
    func upgradeUserUnits()
}

public class UserUnits {
    private struct Keys {
        static let cycleLengthKey = "cycleLengthKey"
        static let periodLengthKey = "periodLengthKey"
    }
    
    public var cycleLength: String{
        get{
            return UserDefaults.standard.string(forKey: Keys.cycleLengthKey) ?? "28"
        }
        set{
            UserDefaults.standard.set(newValue, forKey: Keys.cycleLengthKey)
            delegate?.upgradeUserUnits()
        }
    }
    
    public var periodLength: String{
        get{
            return UserDefaults.standard.string(forKey: Keys.periodLengthKey) ?? "4"
        }
        set{
            UserDefaults.standard.set(newValue, forKey: Keys.periodLengthKey)
            delegate?.upgradeUserUnits()
        }
    }
    
    public static let shared = UserUnits()
    weak var delegate: UserUnitsDelegate?
    
    private init() { }
}
