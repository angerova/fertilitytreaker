import Foundation
import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell{
    
    @IBOutlet weak var substrateLeft: UIView!
    @IBOutlet weak var substrateRight: UIView!
    @IBOutlet weak var substrateRound: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var leftBigView: UIView!
    
    @IBOutlet weak var rightBigView: UIView!
    @IBOutlet weak var rightLittleView: UIView!
    @IBOutlet weak var leftLittleView: UIView!
    
    func handleSelectionEditingMode(cellState: CellState,selectedDatesFromUD: [Date]) {        
        checkImage.isHidden = false
        rightLittleView.isHidden = false
        leftLittleView.isHidden = false
        rightBigView.isHidden = true
        leftBigView.isHidden = true
        roundView.backgroundColor = Colors.whiteColor
        let position = (cellState.isSelected == true) ? checkIsFirstAndLastMonthDay(date: cellState.date, in: selectedDatesFromUD) ?? cellState.selectedPosition(): cellState.selectedPosition()
        let colorsAndImageEditingMode: CellColorsAndImageEditingMode = CellColorsAndImageEditingMode(cellColorsPosition: CellColorsPosition(cellPosition: position), colorTrue: Colors.selectedEditing, colorFalse: .white, imageTrue: #imageLiteral(resourceName: "check"), imageFalse: #imageLiteral(resourceName: "emptyCheck"))
        rightLittleView.backgroundColor = colorsAndImageEditingMode.rightView
        leftLittleView.backgroundColor = colorsAndImageEditingMode.leftView
        checkImage.image = colorsAndImageEditingMode.roundView
        dateLabel.textColor = Colors.blackColor
        let sortedSelected = selectedDatesFromUD.sorted()
        if cellState.date > today!, cellState.date > (sortedSelected.last ?? today!){
            if selectedDatesFromUD.contains(addDayComponent(dayComponent: -1, toDay: cellState.date)){
                checkImage.isHidden = false
            }else{
                checkImage.isHidden = true
            }
            
        }else{
            checkImage.isHidden = false
        }
    }
    
    func handleSelectionViewingMode(cellState: CellState, datesInfoDictionary: [String:DateInformation]) {
        checkImage.isHidden = true
        rightBigView.isHidden = false
        leftBigView.isHidden = false
        rightLittleView.isHidden = true
        leftLittleView.isHidden = true
        
        var position = SelectionRangePosition.none
        var rangeType = RangeType.defaultPeriod
        let dateStr = Formatters.date.string(from: cellState.date)
        if (datesInfoDictionary[dateStr] != nil) {
            
            position = datesInfoDictionary[dateStr]!.position
            rangeType = datesInfoDictionary[dateStr]!.rangeType
        }

        let cellColorsPosition = CellColorsPosition(cellPosition: position)
        
        var colorsViewingMode: CellColorsViewingMode!
        if rangeType == .intersection || rangeType == .intersectionOvulationDay {
            
            colorsViewingMode = CellColorsViewingMode(datesInfoDictionary: datesInfoDictionary, position: position, date: cellState.date)
            
        }else{
            colorsViewingMode = CellColorsViewingMode(cellColorsPosition: cellColorsPosition, rangeType: rangeType)
        }
        if Formatters.date.string(from: Date()) == Formatters.date.string(from: cellState.date){
            colorsViewingMode.roundView = Colors.today
            colorsViewingMode.text = Colors.whiteColor
        }
        if cellState.isSelected{
            colorsViewingMode.roundView = Colors.selectViewing
            colorsViewingMode.text = Colors.whiteColor
        }
        rightBigView.backgroundColor = colorsViewingMode.rightView
        leftBigView.backgroundColor = colorsViewingMode.leftView
        roundView.backgroundColor = colorsViewingMode.roundView
        dateLabel.textColor = colorsViewingMode.text
        roundView.isHidden = false
        if rangeType == .futurePeriod || rangeType == .futureIntersection {
            substrateRound.isHidden = !cellColorsPosition.middle
            substrateLeft.isHidden = !cellColorsPosition.left
            substrateRight.isHidden = !cellColorsPosition.right
            substrateRound.isHidden = (cellState.isSelected) ? true : !cellColorsPosition.middle
            switch position {
            case .full:
                
                rightBigView.isHidden = true
                leftBigView.isHidden = true
                
            case .left:

                 roundView.layer.zPosition = (cellState.isSelected) ? 0 : -1
                rightBigView.isHidden = true
                
            case .right:
                
                roundView.layer.zPosition = (cellState.isSelected) ? 0 : -1
                leftBigView.isHidden = true

            case .middle:
                
                roundView.isHidden = (cellState.isSelected) ? false : true

            default:
                rightBigView.isHidden = true
                leftBigView.isHidden = true
                roundView.isHidden = false
            }
            
            if cellState.date == today!{
                substrateRound.isHidden = true
                roundView.layer.zPosition = 0
            }
            
        }else if rangeType == .futureOvulationDay{
            substrateRound.isHidden = (cellState.isSelected) ? true : false
            substrateLeft.isHidden = true
            substrateRight.isHidden = true
            roundView.isHidden = false
            roundView.layer.zPosition = 0
            
        }else{
            substrateRound.isHidden = true
            substrateLeft.isHidden = true
            substrateRight.isHidden = true
            rightBigView.isHidden = false
            leftBigView.isHidden = false
            roundView.layer.zPosition = 0
            
        }
    }
}



