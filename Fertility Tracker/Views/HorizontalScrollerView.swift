

import Foundation
import UIKit



class HorizontalScrollerView: UIView {
    
    var scrollView = UIScrollView()
    let screenSize: CGRect = UIScreen.main.bounds
    var scrollHeight: CGFloat = 60
    var xPosition: CGFloat!
    var spaceBetweenLabels: CGFloat = 30
    var translucentViewWidth: CGFloat = 60
    var isTitlesAdded = false
    var isEmojiAdded = false
    var userselectedSymptoms: [SymptomsLogData]? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeScrollView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeScrollView()
    }
    
    func initializeScrollView() {
    }
    
    func setupLabelsTogether() {
        scrollView.removeFromSuperview()
        
        let screenWidth = screenSize.width
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: scrollHeight))
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        
       userselectedSymptoms = try? daySymptoms.load(title: Formatters.date.string(from: Date()))
        var labels: [UILabel] = []
        if userselectedSymptoms == nil{
            userselectedSymptoms = []
        }
        
        xPosition = 50
        var titleLabelBG: UIColor = Colors.symptomLabelGreen
        var totalLabelsWidth: CGFloat = 0
        isTitlesAdded = false
        spaceBetweenLabels = 30
        for symptom in userselectedSymptoms! {
            if symptom.emoji != nil{
                let label = EmojiLabel()
                label.setParameters(text: symptom.emoji!)
                totalLabelsWidth = totalLabelsWidth + label.bounds.width
                labels.append(label)

            }else{
                let label = TitleLabel()
                label.setParameters(text: symptom.title, backgroundColor: titleLabelBG, size: 13)
                titleLabelBG = (titleLabelBG == Colors.symptomLabelGreen) ? Colors.symptomLabelYellow : Colors.symptomLabelGreen
                totalLabelsWidth = totalLabelsWidth +  label.bounds.width
                isTitlesAdded = true
                labels.append(label)
            }
            
        }
        
        spaceBetweenLabels = (isTitlesAdded == true) ? 10 : 30
        
        if labels.count != 0{
          totalLabelsWidth = totalLabelsWidth + (spaceBetweenLabels * CGFloat(labels.count - 1))
            xPosition = (totalLabelsWidth <= (screenWidth - translucentViewWidth/2)) ? (screenWidth - totalLabelsWidth)/2 : translucentViewWidth
        }
        for label in labels{
            label.frame = CGRect(x: xPosition, y: (scrollView.bounds.height/2 - label.bounds.height/2), width: label.bounds.width, height: label.bounds.height)
            
            scrollView.addSubview(label)
            xPosition = xPosition + label.bounds.width + spaceBetweenLabels

        }
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: xPosition + translucentViewWidth, height: scrollHeight)
    }
    
    func setupLabels(date: Date, scrollLabels: ScrollLabels) {
        scrollView.removeFromSuperview()
        
        let screenWidth = screenSize.width
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: scrollHeight))
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        var  userselectedSymptoms = try? daySymptoms.load(title: Formatters.date.string(from: date))
        isTitlesAdded = false
        isEmojiAdded = false
        if userselectedSymptoms == nil{
            userselectedSymptoms = []
        }
        xPosition = 25
        spaceBetweenLabels = (scrollLabels == .emoji) ? 10 : 5
        var titleLabelBG: UIColor = Colors.symptomLabelGreen
        for symptom in userselectedSymptoms! {
            if symptom.emoji != nil , scrollLabels == .emoji {
                isEmojiAdded = true
                let label = EmojiLabel()
                label.setParameters(text: symptom.emoji!)
                label.frame = CGRect(x: xPosition, y: (scrollView.bounds.height/2 - label.bounds.height/2), width: label.bounds.width, height: label.bounds.height)
                scrollView.addSubview(label)
                xPosition = xPosition + label.bounds.width + spaceBetweenLabels
            }
            if symptom.emoji == nil , scrollLabels == .title{
                isTitlesAdded = true
                let label = TitleLabel()
                label.setParameters(text: symptom.title, backgroundColor: titleLabelBG, size: 17)
                titleLabelBG = (titleLabelBG == Colors.symptomLabelGreen) ? Colors.symptomLabelYellow : Colors.symptomLabelGreen
                label.frame = CGRect(x: xPosition, y: (scrollView.bounds.height/2 - label.bounds.height/2), width: label.bounds.width, height: label.bounds.height)
                scrollView.addSubview(label)
                xPosition = xPosition + label.bounds.width + spaceBetweenLabels
            }
        }
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: xPosition + translucentViewWidth, height: scrollHeight)
    }
    
}

