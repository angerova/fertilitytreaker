
import Foundation
import UIKit

class EmojiLabel: UILabel{
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        self.font = UIFont(name: "AppleColorEmoji", size: 40)
        self.textAlignment = .center
  
        self.layer.preferredFrameSize()
  
    }
    
    func setParameters(text: String){
        self.text = text
        self.sizeToFit()
        self.frame.size = CGSize(width: self.bounds.width, height: self.bounds.height)
    }
    
    
}


class TitleLabel: UILabel{
    
    var titleLabelHeight: CGFloat = 30
    var lateralTitleLabelSpace: CGFloat = 15
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        self.textAlignment = .center
        self.textColor = .white
        self.frame.size.height = 30
        self.layer.preferredFrameSize()
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
       
        
    }
    
    func setParameters(text: String, backgroundColor: UIColor, size: CGFloat){
        self.text = text
        self.font = UIFont(name: "ArialRoundedMTBold", size: size)
        self.sizeToFit()
        
      
        self.frame.size = CGSize(width: (self.bounds.width + lateralTitleLabelSpace), height: titleLabelHeight)
        
        self.backgroundColor = backgroundColor
    }
    
}
