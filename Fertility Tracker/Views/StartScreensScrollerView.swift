//
//  StartScreensScrollerView.swift
//  Fertility Tracker
//
//  Created by Алина Ангерова on 29/05/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//



import Foundation
import UIKit



class StartScreensScrollerView: UIView {
    
    var scrollView = UIScrollView()
    let screenSize: CGRect = UIScreen.main.bounds
    var xPosition: CGFloat!
    var userDataScreens: [UserDataScreen] = []
    
    var lastDate = Date()
    var periodLength: String = "1"
    var cycleLength: String = "21"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeScrollView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeScrollView()
    }
    
    func initializeScrollView() {
        
        userDataScreens = UserDataScreen.allScreensData()
        
        setupUserDataScreens()
    }
    
    func setupUserDataScreens() {
        scrollView.removeFromSuperview()
        
       
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        scrollView.showsHorizontalScrollIndicator = false
        
        addSubview(scrollView)
        
        
        xPosition = 0
    
        for userScreen in userDataScreens{
            let userDataInputView = UserDataInputView(frame: CGRect(x: xPosition, y: 0, width: screenSize.width, height: screenSize.height))
            userDataInputView.configure(userDataScreen: userScreen)
            userDataInputView.delegate = self
            scrollView.addSubview(userDataInputView)
            xPosition = xPosition + screenSize.width
        }
        xPosition = 0
        
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: xPosition, height: screenSize.height)
    }
    
}


extension StartScreensScrollerView: UserDataInputViewDelegate {
    func setLastDate(_ lastDate: Date) {
        self.lastDate = lastDate
    }
    
    func setPeriodLength(_ periodLength: String) {
        self.periodLength = periodLength
    }
    
    func setCycleLength(_ cycleLength: String) {
        self.cycleLength = cycleLength
    }
    
    func next(_ save: Bool) {
        xPosition = xPosition + screenSize.width
        self.scrollView.setContentOffset(CGPoint(x: xPosition, y: 0), animated: true)
        if save == true{
            userUnits.cycleLength = cycleLength
            userUnits.periodLength = periodLength
            var selectedDates: [Date] = []
            lastDate = Formatters.date.date(from: Formatters.date.string(from: lastDate))!
            for day in 0...(Int(periodLength)! - 1){
                selectedDates.append(addDayComponent(dayComponent: day, toDay: lastDate))
            }
            fertilityRanges.selectedPeriodsUD = selectedDates
            fertilityRanges.reloadDateInfomations()
            NotificationCenter.default.post(name: Notification.Name(rawValue: reloadDateInformationsNotificationKey), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: reloadUnitsNotificationKey), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: showTabBarNotificationKey), object: self)
            self.removeFromSuperview()
        }
    }
}
