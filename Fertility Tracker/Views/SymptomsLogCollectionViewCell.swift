//
//  SymptomsLogCollectionViewCell.swift
//  Fertility Tracker
//
//  Created by Алина Ангерова on 09/04/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import UIKit

class SymptomsLogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var emojiView: UIView!
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var emojiTitleLabel: UILabel!
    @IBOutlet weak var symptomsTitle: UILabel!
    
    var isCellSelected: Bool = false
    
    func configure(symptomsLog: SymptomsLog, indexPath: IndexPath){
       
        switch symptomsLog.name{
        case .symptoms?:
            emojiView.isHidden = true
            symptomsTitle.isHidden = false
            symptomsTitle.text = symptomsLog.symptomsLogData[indexPath.item].title
        default:
            emojiView.isHidden = false
            symptomsTitle.isHidden = true
            emojiLabel.text = symptomsLog.symptomsLogData[indexPath.item].emoji
            emojiTitleLabel.text = symptomsLog.symptomsLogData[indexPath.item].title
        }
    }
}
