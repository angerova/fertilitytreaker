

import Foundation
import UIKit

public protocol DrawerContentViewDelegate: class {
    
    func closeDrawerView()
    func addDetails()
    
}

class DrawerContentView: UIView {
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var chanceLabel: UILabel!
    @IBOutlet weak var dayOfCycleLabel: UILabel!
    
    @IBOutlet weak var emojiScrollerView: HorizontalScrollerView!
    @IBOutlet weak var titleScrollerView: HorizontalScrollerView!
    
    @IBOutlet weak var addDetailsButton: UIButton!
    
    @IBOutlet var translucentLeftView: UIView!
    @IBOutlet var translucentRightView: UIView!
    
    public weak var delegate: DrawerContentViewDelegate?
    
    
    
    @IBAction func addDetails(_ sender: Any) {
        delegate?.addDetails()
    }
    
    @IBAction func close(_ sender: Any) {
        
        delegate?.closeDrawerView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("DrawerContentView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addDetailsButton.layer.masksToBounds = true
        addDetailsButton.layer.cornerRadius = addDetailsButton.bounds.height/2
        addDetailsButton.layer.borderWidth = 1
        addDetailsButton.layer.borderColor = Colors.addDetailsButton.cgColor
        
        let whiteColor: UIColor = .white
        setupGradient(view: translucentLeftView, leftColor: whiteColor.withAlphaComponent(1), middleColor: whiteColor.withAlphaComponent(0.39), rightColor: whiteColor.withAlphaComponent(0))
        setupGradient(view: translucentRightView, leftColor: whiteColor.withAlphaComponent(0), middleColor: whiteColor.withAlphaComponent(0.39), rightColor: whiteColor.withAlphaComponent(1))
    }
    
    func setupLabels(date: Date){
        let dayDescription = DayDescription(date: date)
        dateLabel.text = dayDescription.dateString.uppercased()
        chanceLabel.text = dayDescription.chance
        dayOfCycleLabel.text = dayDescription.cycleDay
    }
    
}
