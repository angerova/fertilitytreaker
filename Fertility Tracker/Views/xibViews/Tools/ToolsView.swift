//
//  ToolsView.swift
//  Fertility Tracker
//
//  Created by Алина Ангерова on 15/05/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import Foundation
import UIKit


class ToolsView: UIView {
    @IBOutlet weak var dayNumberLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ToolsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
    
    
}
