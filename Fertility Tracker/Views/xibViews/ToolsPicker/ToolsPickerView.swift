

import UIKit

public protocol ToolsPickerViewDelegate: class {
    
    func tools(
        _ dayNumber: String
    )
    
    func tools(
        _ done: Bool
    )
    
}

class ToolsPickerView: UIView {
    
    @IBOutlet weak var doneImageView: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var toolsPicker: UIPickerView!
    
    public weak var delegate: ToolsPickerViewDelegate?
    
    var selectedNumber = "4"
    var dayNumbers: [String] = []
    
    @IBAction func done(_ sender: Any) {
        delegate?.tools(true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ToolsPickerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        doneImageView.tintColor = Colors.doneImageViewTintColor
        toolsPicker.delegate = self
        toolsPicker.dataSource = self
    }
    
    func reloadToolsPickerView(data: [String], selectedRow: Int){
        dayNumbers = data
        
        toolsPicker.reloadAllComponents()
        toolsPicker.selectRow(selectedRow, inComponent: 0, animated: false)
    }
}

extension ToolsPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dayNumbers.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = dayNumbers[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: Colors.pickerViewForegroundColor])
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedNumber = dayNumbers[row]
        delegate?.tools(selectedNumber)
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 50
    }
}

