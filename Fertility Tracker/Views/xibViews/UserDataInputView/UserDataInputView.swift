//
//  PeriodLengthView.swift
//  Fertility Tracker
//
//  Created by Алина Ангерова on 29/05/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import Foundation
import UIKit

public protocol UserDataInputViewDelegate: class {
    
    func next(
        _ save: Bool
    )
    
    func setLastDate(
        _ lastDate: Date
    )
    
    func setPeriodLength(
        _ periodLength: String
    )
    
    func setCycleLength(
        _ cycleLength: String
    )
}


class UserDataInputView: UIView {
   
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var pikerHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonTop: NSLayoutConstraint!
    @IBOutlet weak var pickerTop: NSLayoutConstraint!
    @IBOutlet weak var titleTop: NSLayoutConstraint!
    @IBOutlet weak var imageTop: NSLayoutConstraint!
    
    @IBOutlet weak var nextSaveButton: UIButton!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet var contentView: UIView!
    
    var dayNumbers: [String] = []
    var dayNumbersPickerView: UIPickerView!
    var datePickerView: UIDatePicker!
    var name: StartScreens!
    public weak var delegate: UserDataInputViewDelegate?
    
    @IBAction func nextSave(_ sender: Any) {
        let isSave = (name == StartScreens.periodLengthScreen) ? true : false
        delegate?.next(isSave)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("UserDataInputView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        
    }
    
    func configure(userDataScreen: UserDataScreen){
        titleLabel.text = userDataScreen.title
        nextSaveButton.backgroundColor = userDataScreen.buttonColor
        nextSaveButton.setTitle(userDataScreen.buttonText, for: .normal)
        name = userDataScreen.name
        setConstraints()
        if userDataScreen.isDatePicker {
            datePickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width, height: pikerHeight.constant))
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
            pickerView.addSubview(datePickerView)
        }else{
            dayNumbers = userDataScreen.pickerData!
            dayNumbersPickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width, height: pikerHeight.constant))
            dayNumbersPickerView.delegate = self
            dayNumbersPickerView.dataSource = self
            pickerView.addSubview(dayNumbersPickerView)
        }
        
        
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        delegate?.setLastDate(datePickerView.date)
    }
    
    func setConstraints(){
        let screenHeight = contentView.bounds.height
        pikerHeight.constant = screenHeight * 0.39
        buttonTop.constant = screenHeight * 0.05
        pickerTop.constant = screenHeight * 0.04
        titleTop.constant = screenHeight * 0.02
        imageTop.constant = screenHeight * 0.06
    }
    
}


extension UserDataInputView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dayNumbers.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = dayNumbers[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: Colors.pickerViewForegroundColor])
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if name == StartScreens.cycleLengthScreen{
            delegate?.setCycleLength(dayNumbers[row])
        }
        if name == StartScreens.periodLengthScreen{
            delegate?.setPeriodLength(dayNumbers[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 30
    }
}

